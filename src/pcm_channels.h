/* the Music Player Daemon (MPD)
 * Copyright (C) 2008 Max Kellermann <max@duempel.org>
 * This project's homepage is: http://www.musicpd.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef MPD_PCM_CHANNELS_H
#define MPD_PCM_CHANNELS_H

#include <stdint.h>
#include <stddef.h>

const int16_t *
pcm_convert_channels_16(int8_t dest_channels,
			int8_t src_channels, const int16_t *src,
			size_t src_size, size_t *dest_size_r);

const int32_t *
pcm_convert_channels_24(int8_t dest_channels,
			int8_t src_channels, const int32_t *src,
			size_t src_size, size_t *dest_size_r);

#endif
