/* the Music Player Daemon (MPD)
 * Copyright (C) 2008 Max Kellermann <max@duempel.org>
 * This project's homepage is: http://www.musicpd.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef MPD_TAG_POOL_H
#define MPD_TAG_POOL_H

#include "tag.h"

#include <pthread.h>

extern pthread_mutex_t tag_pool_lock;

struct tag_item;

struct tag_item *tag_pool_get_item(enum tag_type type,
				   const char *value, int length);

struct tag_item *tag_pool_dup_item(struct tag_item *item);

void tag_pool_put_item(struct tag_item *item);

#endif
