/* the Music Player Daemon (MPD)
 * Copyright (C) 2008 Max Kellermann <max@duempel.org>
 * This project's homepage is: http://www.musicpd.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "output_api.h"
#include "output_internal.h"

#include <assert.h>

const char *audio_output_get_name(const struct audio_output *ao)
{
	return ao->name;
}

void audio_output_closed(struct audio_output *ao)
{
	assert(ao->open);

	ao->open = false;
}

bool audio_output_is_pending(const struct audio_output *ao)
{
	return ao->command != AO_COMMAND_NONE;
}
